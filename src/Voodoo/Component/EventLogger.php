<?php

/**
 * An Event logger. Requires Raven_Client from Sentry (getsentry.com)
 * @link https://github.com/getsentry/raven-php
 * 
 * @name EventLogger
 * @author Mardix
 * @since  Feb 11 2014
 */

namespace Voodoo\Component;

use 
    Voodoo,
    Exception,
    ErrorException,
    Raven_Client,    
    Raven_ErrorHandler;

class EventLogger
{
    const DEBUG = 'debug';
    const INFO = 'info';
    const WARN = 'warning';
    const WARNING = 'warning';
    const ERROR = 'error';
    const FATAL = 'fatal';

    private $eventId = null;
    private $_ravenClient = null;
    private static $ravenClient = null;
    private $sentryDsn = null;
    private $sentryParams = [];
    
    public function __construct($sentryDsn = null)
    {
        $this->sentryParams = [
            "logger" => "php",
            "tags" => [
                    "hostname" => Voodoo\Core\Env::getHostName(),
                    "voodoophp_version" => Voodoo\Core\Application::VERSION,                   
                ]
            ];
        $this->sentryDsn = $sentryDsn;
    }
    
    /**
     * Return the client
     * 
     * @return Raven_Client
     */
    public function getClient()
    {
        if ($this->sentryDsn) {
            if (!$this->_ravenClient) {
                $this->_ravenClient = new Raven_Client($this->sentryDsn, $this->sentryParams);
            }
            return $this->_ravenClient;
        } else {
            if (! self::$ravenClient) {
                $dsn = Voodoo\Core\Config::Component()->get("EventLogger.dsn");
                self::$ravenClient = new Raven_Client($dsn, $this->sentryParams);
            }
            return self::$ravenClient;            
        }
    }
    
    
    /**
     * Log message
     * 
     * @param type $message
     * 
     * @param array $params
     * @param type $level
     * @return \Voodoo\Component\Logger\Event
     */
    public function message($message, $params = [], $level = self::INFO)
    {
        $this->eventId = $this->getClient()
                                ->captureMessage($message, $params, $level);
        return $this;
    }

    /**
     * Capture the query
     * 
     * @param string $query
     * @param string $engine
     * @param sting $level
     * 
     * @return \Voodoo\Component\Logger\Event
     */
    public function query($query, $engine = "", $level = self::DEBUG)
    {
        $this->eventId = $this->getClient()
                                ->captureQuery($query, $level, $engine);
        return $this;        
    }
    
    /**
     * Log an exception
     * 
     * @param Exception $e
     * @return \Voodoo\Component\Logger\Log
     */
    public function exception(Exception $e)
    {
        $this->eventId = $this->getClient()
                                ->captureException($e);
        return $this;
    }    

    public function error($message, $code = 0, $file = '', $line = 0)
    {
        $this->exception(new ErrorException($message, 0, $code, $file, $line));
        return $this;
    }

    /**
     * Return the event Id
     * 
     * @return string
     */
    public function getId()
    {
        return $this->eventId;
    }
    
    
    public function setTags(Array $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * Catch all errors
     * @param bool $registerGenericError - to register generic error. It will be noisey
     */
    public static function catchAllErrors($registerGenericError = false)
    {
        $error_handler = new Raven_ErrorHandler((new self)->getClient());
        $error_handler->registerExceptionHandler();
        $error_handler->registerShutdownFunction();
        
        if ($registerGenericError) {
            $error_handler->registerErrorHandler();
        }
    }  
}
